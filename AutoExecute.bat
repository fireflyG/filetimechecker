@echo off
REM TASK1: Fetch the current and previous trade date
echo begin TASK1: Fetch trade dates
set path=D:\FileModifiedTimeChecker\TradeDayFetch\bin\Debug
cd %path%
set filename="Configuration.xml"
TradeDayFetch.exe configfile:%filename%
echo TASK1 Finished at: %date% : %time%
echo.


REM TASK2: Launch Time Checker Program
echo begin Task2: Check db modified time
set path=D:\FileModifiedTimeChecker\FileTimeChecker\bin\Debug
cd  %path%
set /A lpcnt=0
:RunChecker
set filename=FileConfig.xml
FileTimeChecker.exe configfile:%filename%
if %lpcnt% gtr 28 goto ShortCircuit
if %ERRORLEVEL% NEQ 1 ( 
	set /A lpcnt=%lpcnt% + 1
	C:\Windows\System32\TIMEOUT /t 900
	goto RunChecker
)
echo Task2 finished, Write into XML config file at: %date% : %time%
echo.

REM TASK3: Copy database from dropbox to local
echo begin task3, Copy dropbox database file to local
%systemroot%\System32\XCOPY "D:\Dropbox (X-FA T1)\HDNDBNew\*" "C:\HDNDBNew" /S /I /R /Y
echo Task3 finished Database has been copied at : %date% : %time%
echo.

REM Batch File Exit 1
echo All tasks finished, End
cd %~dp0
@echo 1 > Status.txt 
exit

REM Batch File Exit 2
:ShortCircuit
echo.
echo Database File are not updated, check process timeout, databases are not copyed. END
cd %~dp0
@echo 0 > Status.txt 
exit



