﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace EmailNotifier
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = new ConfigManager(@"Configuration.xml");
            var email = new EmailService(config);
            email.Send();
        }
    }
}
