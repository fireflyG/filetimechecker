﻿
using MimeKit;
using MailKit.Net.Smtp;
using System.IO;

namespace EmailNotifier
{
    class EmailService
    {
        private ConfigManager configer;

        public EmailService(ConfigManager _configer)
        {
            configer = _configer;     
        }

        public void Send()
        {
           var message =  this.ComposeMessage();

            using (var client = new SmtpClient())
            {
                client.Connect("smtp.office365.com", 587, false);
                client.Authenticate(configer.GetAccount(), configer.GetPassword());
                client.Send(message);
                client.Disconnect(true);
            }            

        }

        private MimeMessage ComposeMessage()
        {
            var msg = new MimeMessage();

            msg.From.Add(new MailboxAddress("Lev_Office_Bot", configer.GetFromWhom()));

            msg.To.Add(new MailboxAddress("DB_Observer", configer.GetToWhom()));

            msg.Subject = configer.GetSubject();

            var body = new TextPart("plain")
            {
                Text = configer.GetEmailText()
            };

            var path = configer.GetAttachmentPath();
            var attachment = new MimePart() {
                Content = new MimeContent(File.OpenRead(path), ContentEncoding.Default),
                ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                ContentTransferEncoding = ContentEncoding.Base64,
                FileName = Path.GetFileName(path)
            };

            var multipart = new Multipart("mixed");
            multipart.Add(body);
            multipart.Add(attachment);

            msg.Body = multipart;

            return msg;
        }
    }
}
