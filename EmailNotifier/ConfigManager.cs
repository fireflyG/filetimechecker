﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailNotifier
{
    class ConfigManager
    {
        private string configfile;
        private Dictionary<string, string> configValue;

        public ConfigManager(string _file)
        {
            configfile = _file;
            if (File.Exists(_file))
            {
                Load(configfile);    
            }
            else
            {
                Console.WriteLine("Cannot find the config file, check the xml file path");
                throw new FileLoadException();
            }

        }

        private void Load(string file)
        {
            configValue = new Dictionary<string, string>();

            XDocument config = XDocument.Load(file);

            foreach(var item in config.Root.Element("Task1").Elements())
            {
                if (!configValue.ContainsKey(item.Attribute("key").Value)) ;
                configValue.Add(item.Attribute("key").Value, item.Attribute("value").Value);
            }
        }

        public string GetToWhom()
        {
            return configValue["To"].ToString();
        }

        public string GetFromWhom()
        {
            return configValue["From"].ToString();
        }

        public string GetEmailText()
        {
            return configValue["TextContent"].ToString();
        }

        public string GetAttachmentPath()
        {
            return configValue["Attachment"].ToString();
        }

        public string GetAccount()
        {
            return configValue["Account"].ToString();
        }

        public string GetPassword()
        {
            return configValue["Password"].ToString();
        }

        public string GetSubject()
        {
            return configValue["Subject"].ToString();
        }
    }
}
