@echo off
set /a lp=0
:CheckStatus
cd %~dp0
set /p st=<Status.txt
set /a st=%st% + 0
if %lp% gtr 28 goto ShortCircuit
if %st% NEQ 1 (
	set /a lp=%lp% + 1
	C:\Windows\System32\TIMEOUT /t 900
	goto CheckStatus
)
:SendEmail
REM REM email send
set path=D:\FileModifiedTimeChecker\EmailNotifier\bin\Debug
cd %path%
EmailNotifier.exe
cd %~dp0
@echo 0 > Status.txt
exit

:ShortCircuit
REM REM email not send
cd %~dp0
@echo 0 > Status.txt
exit