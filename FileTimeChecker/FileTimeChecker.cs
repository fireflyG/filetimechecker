﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace FileModifiedTimeChecker
{
    class FileModifiedTimeChecker
    {

        private ConfigManager config;

        private Dictionary<string, DateTime> _fileTimeDict;

        private Dictionary<string, bool> _compareResults;

        public FileModifiedTimeChecker(ConfigManager _configer)
        {
            config = _configer;
            _fileTimeDict = _configer.GetFileGroupTimeDict();
            _compareResults = _configer.GetCompareResultDict();
        }

        public int IsModified()
        {
            foreach(var filePath in _fileTimeDict.Keys)
            {
                var fileSysTime = File.GetLastWriteTime(filePath);
                var fileXMLTime = _fileTimeDict[filePath];
                bool isChanged = Math.Abs(fileSysTime.Subtract(fileXMLTime).TotalSeconds) > 1;
                string key = GetNameFromPath(filePath);
                _compareResults[key] = isChanged;

            }

            bool allUpdate = true;
            foreach(var compare in _compareResults.Values)
            {
                allUpdate = allUpdate && compare;
            }

            if (allUpdate)
            {
                UpdateXMLFile();
                return 1;
            }
            else
            {
                foreach(var key in _compareResults.Keys)
                {
                    if (!_compareResults[key])
                    {
                        Console.WriteLine(key + " is not modified");
                    }
                }
                return 0;
            }

        }

        private void UpdateXMLFile()
        {
            List<string> keys = new List<string>();
            foreach(var key in _fileTimeDict.Keys)
            {
                keys.Add(key);
            }

            foreach(var filePath in keys)
            {
                var fileSysTime = File.GetLastWriteTime(filePath);
                _fileTimeDict[filePath] = fileSysTime;
            }

            config.UpdateDateTimesToXML(_fileTimeDict);
        }

        private string GetNameFromPath(string path)
        {
            var length = path.Length - 1 - path.LastIndexOf('\\') - 1;
            return path.Substring(path.LastIndexOf('\\') + 1, length);
        }
    }
}
