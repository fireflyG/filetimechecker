﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Linq;

namespace FileModifiedTimeChecker
{
    class ConfigManager
    {

        private string _configFile;

        private Dictionary<string, DateTime> fileGroupConfigValue;

        private Dictionary<string, bool> compareResultDict;

        public ConfigManager(string configFilePath)
        {
            _configFile = configFilePath;
            if (File.Exists(_configFile))
            {
                LoadConfigFile(_configFile);
            }
            else
            {
                Console.WriteLine("Cannot find the config file, check the path and file name");
                throw new FileLoadException();
            }
        }

        //Loading  XML Config file, specifically, load file-last modified time pair into dictionary
        private void LoadConfigFile(string fileName)
        {
            fileGroupConfigValue = new Dictionary<string, DateTime>();

            XDocument config = XDocument.Load(fileName);

            foreach (var item in config.Root.Element("Task1").Elements())
            {
                string key = "";
                string value = "";
                foreach (var subitem in item.Elements())
                {
                    switch (subitem.Attribute("key").Value)
                    {
                        case "FilePath":
                            {
                                key = subitem.Attribute("value").Value;
                                break;
                            }
                        case "CompareTime":
                            {
                                value = subitem.Attribute("value").Value;
                                break;
                            }
                        default:
                            {
                                Console.WriteLine("Check FileConfig.xml file, format is not correct");
                                throw new ArgumentException();
                            }
                    }
                }

                fileGroupConfigValue.Add(key, DateTime.Parse(value));
            }


            compareResultDict = new Dictionary<string, bool>();

            foreach (var item in config.Root.Element("Task1").Elements())
            {
                string key = "";
                bool value = false;
                foreach (var subitem in item.Elements())
                {
                    if(subitem.Attribute("key").Value == "FilePath")
                    {
                        key = GetNameFromPath(subitem.Attribute("value").Value);
                    }
                }
                compareResultDict.Add(key, value);
            }

        }

        private string GetNameFromPath(string path)
        {
            var length = path.Length - 1 - path.LastIndexOf('\\') - 1;
            return path.Substring(path.LastIndexOf('\\') + 1, length);
        }

        public Dictionary<string, bool> GetCompareResultDict()
        {
            return this.compareResultDict;
        }

        public Dictionary<string, DateTime> GetFileGroupTimeDict()
        {
            return this.fileGroupConfigValue;
        }

        //Update XML File, using Passed in file-time dictionary update corresponding xml file element
        public void UpdateDateTimesToXML(Dictionary<string, DateTime> UpdatedInfo)
        {
            XDocument doc = XDocument.Load(_configFile);

            foreach (var file in UpdatedInfo.Keys)
            {
                //Loop through all "CheckTarget" xml Elements
                foreach (var checkTarget in doc.Root.Element("Task1").Elements())
                {
                    //Check <FilePath> el, Write <CompareTime> el in the CheckTarget Element 
                    foreach (var subItem in checkTarget.Elements())
                    {
                        if (subItem.Attribute("key").Value == "FilePath" &&
                            subItem.Attribute("value").Value != file)
                        {
                            break;
                        }

                        if (subItem.Attribute("key").Value == "CompareTime")
                        {
                            var ts = UpdatedInfo[file].ToString("MM/dd/yyyy h:mm:ss tt");
                            subItem.Attribute("value").SetValue(ts);
                        }

                    }
                }
            }
            doc.Save(_configFile);
        }

    }
}
