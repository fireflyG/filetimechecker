﻿using System;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;

namespace FileModifiedTimeChecker
{
    class Program
    {
        static int Main(string[] args)
        {
            Dictionary<string, string> arguments = args.Select(x => x.Split(':')).ToDictionary(x => x.First(), x => x.Last());

            string file = arguments.ContainsKey("configfile") ? arguments["configfile"] : @"FileConfig.xml";

            var configManager = new ConfigManager(file);

            FileModifiedTimeChecker checker = new FileModifiedTimeChecker(configManager);

            var isAllModified = checker.IsModified();

            Console.Write("all file is modified: " + (checker.IsModified() == 1? "TURE!" : "FALSE"));

            return isAllModified;
        }
    }
}
 