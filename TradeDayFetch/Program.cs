﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeDayFetch
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, string> arguments = args.Select(x => x.Split(':')).ToDictionary(x => x.First(), x => x.Last());

            string file = arguments.ContainsKey("configfile") ? arguments["configfile"] : @"Configuration.xml";

            var configManager = new ConfigManager(file);

            var tdFinder = new TradeDayFinder(configManager);

            tdFinder.Write_TadeDay_ToTxt();
        }
    }
}
