﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.IO;

namespace TradeDayFetch
{
    class TradeDayFinder
    {
        private ConfigManager configer;

        public TradeDayFinder(ConfigManager _configer)
        {
            configer = _configer;
        }

        public void Write_TadeDay_ToTxt()
        {

            string _connStr = configer.ConnectionString();

            string current_file_path = configer.CurrFilePath();

            string prev_file_path = configer.PrevFilePath();

            string sql = configer.QueryString(DateTime.Now);

            List<DateTime> ret = new List<DateTime>();

            using (OleDbConnection conn = new OleDbConnection(_connStr))
            {
                using (OleDbCommand command = new OleDbCommand(sql, conn))
                {
                    conn.Open();
                    using (OleDbDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var idx = reader.GetOrdinal("TradeDate");
                            ret.Add(reader.IsDBNull(idx) ? default(DateTime) : reader.GetDateTime(idx));
                        }
                    }
                }
            }

            if (ret.Count != 2)
            {
                Console.WriteLine("Something wrong about the trade dates|| Find Trade Date Task");
                throw new Exception();
            }

            var cur = ret[0];
            var prev = ret[1];

            if (cur < prev)
            {
                Console.WriteLine("Check the SQL QUERY ORDER BY CLAUSE|| Find Trade Date Task");
                var temp = cur;
                cur = prev;
                prev = temp;
            }

            File.WriteAllText(current_file_path, cur.ToString("MM/dd/yyyy"));

            File.WriteAllText(prev_file_path, prev.ToString("MM/dd/yyyy"));
        }
    }
}
