﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Linq;

namespace TradeDayFetch
{
    class ConfigManager
    {
        private string _configFile;

        private Dictionary<string, string> configValue_task1;

        public ConfigManager(string _filePath)
        {
            _configFile = _filePath;
            if (File.Exists(_configFile))
            {
                Load(_configFile);
            }
            else
            {
                Console.WriteLine("Cannot find the config file, check the path and file name");
                throw new FileLoadException();
            }
        }

        private void Load(string fileName)
        {
            configValue_task1 = new Dictionary<string, string>();

            XDocument config = XDocument.Load(fileName);

            foreach (var item in config.Root.Element("Task1").Elements())
            { 
                var key = item.Attribute("key").Value;
                var val = item.Attribute("value").Value;

                configValue_task1.Add(key, val);
            }
        }

        public string ConnectionString()
        {
            return configValue_task1["ConnectionString"];
        }

        public string QueryString(DateTime cursor)
        {
            return string.Format(configValue_task1["queryString"], cursor.ToString());
        }

        public string CurrFilePath()
        {
            return configValue_task1["CurrentTDFilePath"];
        }

        public string PrevFilePath()
        {
            return configValue_task1["PrevTDFilePath"];
        }

    }
}
